'''
INPUT
x1, x2, x3 ...
HIDDEN LAYER OF NEURONS:
w1, w2, w3, w4 (every output goes into all layer)
IF THERE IS 2 LAYERS OR MORE, ITS DEEP NN
OUTPUT
cat or dog ... (Two neurons)


sum of inputs (x1, x2, ...) 
activation fc if x1 
we will work between 0 & 1 -> sigmoid act func
output:
    0.79 its dog
    0.21 its cat
'''

import tensorflow as tf
import matplotlib.pyplot as plt
import tensorflow.keras
from keras import models
import numpy as np

mnist = tf.keras.datasets.mnist # 28x28 hand-written imgs (0-9)

# split the data
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# cm.binary makes it in two colors (black and white)
plt.imshow(x_train[0], cmap = plt.cm.binary) # show the first pic
print(x_train[0]) # thats how img[0] looks in the code

# change the data of pics to values between 0 and 1
x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1)

# create a model
model = models.Sequential()
model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu)) #1st layer of neurons
# 128=num of neurons in one layer, activation relu func
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu)) #2nd
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax)) #output -> softmax probability

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy']) # model improve by decrease the loss!

# fit the model for train data
# epoch means, how many times the model will see training data
model.fit(x_train, y_train, epochs=3)

# control if model does not overfit (we add y_test) and print acc & loss
val_loss, val_acc = model.evaluate(x_test, y_test)
print(f'{val_loss}, {val_acc}')

# we can save model
model.save('myFirstModel')
# we can load model
new_model = tf.keras.models.load_model('myFirstModel')

#we can predict
predictions = new_model.predict([x_test])
print(np.argmax(predictions[234])) # will show prediction of 0 element

#we can look if program is correct
plt.imshow(x_test[234])
plt.show()



