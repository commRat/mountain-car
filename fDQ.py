'''
Output is a action (if there are 3 max actions, 
agent has to pick one).
'''
from keras.models import Sequential
from keras.layer import Dense, Dropout, Conv2D, Activation, Flatten, MaxPooling2D
from keras.callbacks import TensorBoard
from keras.optimizers import Adam


class DQNAgent:
    def __init__(self):
        self.model = self.create_model()
    
    def create_model(self):
        model = Sequential()
        model.add(Conv2D(256, (3,3), input_shape = env.OBSERVATION_SPACE_VALUES))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(2,2))
        model.add(Dropout(0.2))

        model.add(Conv2D(256, (3,3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(2,2))
        model.add(Dropout(0.2))

        model.add(Flatten())
        model.add(Dense(64))
        
        model.add(Dense(ev.ACTIVATION_SPACE_SIZE, activation='linear'))
        model.compie(loss='mse', optimizer=Adam(lr=0.001), metrics=['accuracy'])
        return model



